const cheerio = require("cheerio");
const axios = require("axios");
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');
const client = new Discord.Client();

var updateChannel = ''
var siteName = ''
var siteUrl = "https://steamcommunity.com/sharedfiles/filedetails/changelog/1803395040";

client.once('ready', () => {
  console.log('Ready!');
});

// MESSAGE RECEIVED
client.on('message', message => {
  console.log(message.content);

if (!message.content.startsWith(prefix) || message.author.bot) return;

const args = message.content.slice(prefix.length).split(' ');
const command = args.shift().toLowerCase();

//SET MOD ID
if (command === 'set-mod-id') {

    if (!args.length) {
    return message.channel.send(`You didn't provide any mod id, ${message.author}!`);
  }

  siteName = '';
  siteUrl = "https://steamcommunity.com/sharedfiles/filedetails/changelog/" + args[0];

  getResults().then(function(results) {
      
      if(siteName != '') {
        return message.channel.send(`New Workshop Item Set - ${siteName}`);
      }else {
        return message.channel.send(`Workshop ID Not Found`);
      }
  });

      //SET PATCH NOTES CHANNEL
}else if (command === 'set-patch-notes-channel') {
  if (!args.length) {
    return message.channel.send(`You didn't provide any arguments, ${message.author}!`);
  }
    let room1 = client.channels.find(x => x.name === args[0]);
    if(room1 == null) {
      return message.channel.send(`Could not find room id for : ${args}`);
    }
    updateChannel = room1;
    message.channel.send(`Patch note updates will be sent to ${room1}`);

    //GET LAST UPDATE
    }else if (command === 'last-update') {

  getResults().then(function(results) {
      var it = updates.values();
      var first = it.next();
      var val = first.value;
      removeNL(val);
      val = val.replace(/\s{2,}/g, ' ');
       return message.channel.send(`${val}`);
  });

}
});

client.login(token)

updatesPrevious = new Set();
updates = new Set();

const fetchData = async () => {
  const result = await axios.get(siteUrl);
  return cheerio.load(result.data);
};

const getResults = async () => {
  const $ = await fetchData();

  siteName = $('.workshopItemTitle').text();

  $(".detailBox.noFooter").each((index, element) => {
    updates.add($(element).text());
  });
  return {
    updates: [...updates].sort(),
    siteName
  };
};

function removeNL(s){ 
  return s.replace(/[\n\r\t]/g,); 
}

function intervalFunc() {

  const getResults = async () => {
  const $ = await fetchData();

  siteName = $('.workshopItemTitle').text();

  $(".detailBox.noFooter").each((index, element) => {
    updates.add($(element).text());
  });
  return {
    updates: [...updates].sort(),
    siteName
  };
};

var it = updates.values();
var first = it.next();

if (updates != updatesPrevious) {
  updatesPrevious = updates;
}
if(updateChannel.length) {
  console.log(updates);
  client.channels.get(updateChannel).send(updates[0])
}
console.log("Checked for updates")

getResults();
}

setInterval(intervalFunc, 30000);

module.exports = getResults;
